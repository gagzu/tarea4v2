/* 5000304 6182649 */
#include "../include/info.h"
#include "../include/cadena.h"

#include <stddef.h>
#include <stdio.h>
#include <assert.h>

struct nodo {
	info_t dato;
	nodo *anterior;
	nodo *siguiente;
};
struct rep_cadena {
	nodo *inicio;
	nodo *final;
};
cadena_t crear_cadena(){
	cadena_t res = new rep_cadena;
	res->inicio = res->final = NULL;
	return res;
}
cadena_t insertar_al_final(info_t i, cadena_t cad) {
	nodo *nuevo = new nodo;
	nuevo->dato = i;
	nuevo->siguiente = NULL;
	nuevo->anterior = cad->final;
	if (es_vacia_cadena(cad)) {
		cad->inicio = nuevo;
	} else {
		cad->final->siguiente = nuevo;
	}
	cad->final = nuevo;
	return cad;
}
cadena_t insertar_antes(info_t i, localizador_t loc, cadena_t cad) {
	if (localizador_en_cadena(loc, cad)) {
		nodo* nueva = new nodo;
		nueva->dato = i;
		nueva->siguiente = loc;
		if (loc->anterior != NULL){
			nueva->anterior = loc->anterior;
			loc->anterior->siguiente = nueva;
		} else{
			nueva-> anterior = NULL;
			cad->inicio= nueva;
		}
		loc->anterior = nueva;
	}
	return cad;
}
cadena_t insertar_segmento_despues(cadena_t &sgm, localizador_t loc, cadena_t cad){
	assert(es_vacia_cadena(cad) || localizador_en_cadena(loc, cad));
	if (es_vacia_cadena(cad)) {
		cad->inicio = sgm->inicio;
		cad->final = sgm->final;
	} else {
		if (!es_vacia_cadena(sgm)) {
			sgm->inicio->anterior = loc;
			sgm->final->siguiente = loc->siguiente;
			if (es_final_cadena(loc, cad))
				cad->final = sgm->final;
			else
				loc->siguiente->anterior = sgm->final;
			loc->siguiente = sgm->inicio;
		}
	}
	sgm->inicio = sgm->final = NULL;
    return cad;
}
cadena_t copiar_segmento(localizador_t desde, localizador_t hasta,
                         cadena_t cad){
	assert(es_vacia_cadena(cad) || precede_en_cadena(desde, hasta, cad));
	cadena_t res = crear_cadena();
	if (!es_vacia_cadena(cad)) {
		localizador_t loc = desde;
		while(loc != siguiente(hasta, cad)) {
			//ERROR: compartiria memoria
			// info_t info = loc-> dato;
			info_t info = copia_info((loc->dato));
			insertar_al_final(info, res);
			loc = siguiente(loc, cad);
		}
	}
	return res;
}
cadena_t cortar_segmento(localizador_t desde, localizador_t hasta, cadena_t cad){
	/* Si 'desde' está antes de 'hasta' y la cadena no es vacía cortamos el segmento */
	if (!es_vacia_cadena(cad) && precede_en_cadena(desde, hasta, cad)) {
		rep_cadena* segmento = crear_cadena();
		segmento->inicio = desde;
		segmento->final = hasta;

		/* Separo en casos dependiendo de donde arranca y termina */
		if (es_inicio_cadena(desde, cad) && es_final_cadena(hasta, cad)) {
			cad->inicio = cad->final = NULL;
		} else if (es_inicio_cadena(desde, cad) && !es_final_cadena(hasta, cad)) {
			cad->inicio = hasta->siguiente;
			cad->inicio->anterior = NULL;
			segmento->final->siguiente = NULL;
		} else if (!es_inicio_cadena(desde, cad) && es_final_cadena(hasta, cad)) {
			cad->final = desde->anterior;
			cad->final->siguiente = NULL;
			segmento->inicio->anterior = NULL;
		} else {
			desde->anterior->siguiente = hasta->siguiente;
			hasta->siguiente->anterior = desde->anterior;
			segmento->inicio->anterior = segmento->final->siguiente = NULL;
		}
  	liberar_cadena(segmento);
	}
	return cad;
}
cadena_t remover_de_cadena(localizador_t &loc, cadena_t cad) {
	assert(localizador_en_cadena(loc, cad));
	if (es_inicio_cadena(loc, cad) && es_final_cadena(loc, cad)){
	/* Si la cadena esta formada por un solo nodo */
		cad->inicio = NULL;
		cad->final = NULL;
	} else if (es_inicio_cadena(loc, cad)){
		/* Si el nodo es el inicio de la cadena */
		cad->inicio = loc->siguiente;
		loc->siguiente->anterior = NULL;
	} else if (es_final_cadena(loc, cad)){
		/* Si el nodo es el final de la cadena */
		cad->final = loc->anterior;
		loc->anterior->siguiente = NULL;
	} else {
		loc->anterior->siguiente = loc->siguiente;
		loc->siguiente->anterior = loc->anterior;
	}
	/* Liberamos la memoria que se pidio para almacenar el dato del nodo */
	liberar_info(loc->dato);
	delete loc; /* Loc queda indeterminado */
  return cad;
}
cadena_t liberar_cadena(cadena_t cad){
	nodo *a_borrar;
	while (cad->inicio != NULL) {
		a_borrar = cad->inicio;
		cad->inicio = cad->inicio->siguiente;
		liberar_info(a_borrar->dato);
		delete (a_borrar);
	}
	delete cad;
    return cad;
}
bool es_localizador(localizador_t loc) {
	return loc != NULL;
}
bool es_vacia_cadena(cadena_t cad){
	return (cad->inicio == NULL) && (cad->final == NULL);
}
bool es_final_cadena(localizador_t loc, cadena_t cad){
	return !es_vacia_cadena(cad) && final_cadena(cad) == loc;
}
bool es_inicio_cadena(localizador_t loc, cadena_t cad){
	return !es_vacia_cadena(cad) && inicio_cadena(cad) == loc;
}
bool localizador_en_cadena(localizador_t loc, cadena_t cad){
	if (es_vacia_cadena(cad)) {return false;}
	if (loc == NULL) {return false;}
	nodo* aux = inicio_cadena(cad);
	while(aux != NULL) {
		if (aux == loc)
			return true;
		aux = aux->siguiente;
	}
	return false;
}

bool precede_en_cadena(localizador_t loc1, localizador_t loc2, cadena_t cad){
	localizador_t cursor = loc1;
	while (es_localizador(cursor) && (cursor != loc2))
		cursor = siguiente(cursor, cad);
	return ((cursor == loc2) && (localizador_en_cadena(loc1, cad)));
}

localizador_t inicio_cadena(cadena_t cad){
	return cad->inicio;
}
localizador_t final_cadena(cadena_t cad){
	return cad->final;
}
localizador_t kesimo(nat k, cadena_t cad){
	if (k==0){
		return NULL;
	}
	nat contador = 1;
	nodo* aux = inicio_cadena(cad);
	while((aux != NULL)&&(contador < k)){
		aux = siguiente(aux,cad);
		contador++;
	}
	return aux;

}
localizador_t siguiente(localizador_t loc, cadena_t cad){
	return loc->siguiente;
}
localizador_t anterior(localizador_t loc, cadena_t cad){
	return loc->anterior;
}
localizador_t menor_en_cadena(localizador_t loc, cadena_t cad){
	assert(localizador_en_cadena(loc, cad));
	localizador_t res = loc;
	while (es_localizador(siguiente(loc, cad))) {
		loc = loc->siguiente;
		if (numero_info(info_cadena(loc, cad)) < numero_info(info_cadena(res, cad)))
			res = loc;
	}
	return res;
}
localizador_t siguiente_clave(int clave, localizador_t loc, cadena_t cad){
	assert(es_vacia_cadena(cad) || localizador_en_cadena(loc, cad));
	localizador_t res = loc;
	if (es_vacia_cadena(cad))
		res = NULL;
	else {
		while (es_localizador(res) && numero_info(info_cadena(res, cad)) != clave)
			res = siguiente(res, cad);
	}
	return res;
}
info_t info_cadena(localizador_t loc, cadena_t cad){
	assert(localizador_en_cadena(loc, cad));
	return loc->dato;
}
cadena_t cambiar_en_cadena(info_t i, localizador_t loc, cadena_t cad){
	assert(localizador_en_cadena(loc, cad));
	loc->dato = i;
    return cad;
}
cadena_t intercambiar(localizador_t loc1, localizador_t loc2, cadena_t cad){
	assert(localizador_en_cadena (loc1, cad) && localizador_en_cadena (loc2, cad));
	info_t aux = loc2->dato; 
	loc2->dato = loc1->dato;
	loc1->dato = aux;
    return cad;
}
void imprimir_cadena(cadena_t cad){
	nodo* aux = cad->inicio;
	while (aux != NULL) {
		printf("(%i,%s)", numero_info(aux->dato), frase_info(aux->dato));
		aux = aux->siguiente;
	}
	printf("\n");
}




