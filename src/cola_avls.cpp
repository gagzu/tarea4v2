/*
  Módulo de definición de `cola_avls_t'.

  `cola_avls_t' es una estructura lineal con comportamiento FIFO cuyos
   elementos son de tipo `avl'.

  Laboratorio de Programación 2.
  InCo-FIng-UDELAR
 */

#include "../include/cola_avls.h"
#include <stdlib.h>

// Representación de `cola_avls_t'.
// Se debe definir en cola_avls.cpp

struct nodo_cola{
    avl_t arbol;
    nodo_cola *sig;
};

typedef nodo_cola* nodo;

struct rep_cola_avls{
    nodo_cola *primero, *ultimo;
};

/*  Devuelve una cola_avls_t vacía (sin elementos). */
cola_avls_t crear_cola_avls(){
    cola_avls_t result = new rep_cola_avls;
    result->primero = NULL;
    result->ultimo = NULL;
    return result;
}

/* Encola `avl' en `c'. */
void encolar(avl_t b, cola_avls_t &c){
    nodo nuevo = new nodo_cola;
    nuevo->arbol = b;
    nuevo->sig = NULL;
    if (c->primero == NULL)
        c->primero = nuevo;
    else
        c->ultimo->sig = nuevo;
    c->ultimo = nuevo;
}

/*
  Remueve de `c' el elemento que está en el frente.
  NO libera la memoria del elemento removido.
  Si es_vacia_cola_binarios(c) no hace nada.
 */
void desencolar(cola_avls_t &c){
    if (!es_vacia_cola_avls(c)){
        nodo temp = c->primero;
        c->primero = c->primero->sig;
        if (c->primero == NULL)
            c->ultimo = NULL;
        delete temp;
    }
}

/* Libera la memoria asignada a `c', pero NO la de sus elementos. */
void liberar_cola_avls(cola_avls_t &c){
    nodo temp;
    while(c->primero != NULL){
        temp = c->primero;
        c->primero = c->primero->sig;
        delete temp;
    }
    delete c;
}

/* Devuelve `true' si y sólo si `c' es vacía (no tiene elementos). */
bool es_vacia_cola_avls(cola_avls_t c){
    return c->primero == NULL;
}

/*
  Devuelve el elemento que está en el frente de `c'.
  Precondición: ! es_vacia_cola_binarios(c);
 */
avl_t frente(cola_avls_t c){
    return c->primero->arbol;
}