/* 5000304 6182649 */
#include "../include/info.h"
#include "../include/cadena.h"
#include "../include/uso_cadena.h"

#include <stddef.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

bool pertenece(int i, cadena_t cad){
	if (es_vacia_cadena(cad)){
		return false;
	}
	nodo* aux = inicio_cadena(cad);
	while (aux != NULL){
		if (numero_info(info_cadena(aux, cad)) == i){
			return true;
		} else {
			aux = siguiente(aux, cad);
		}
	}
	return false;
}
nat longitud(cadena_t cad){
	if (es_vacia_cadena(cad)){
		return 0;
	}
	nodo* aux = inicio_cadena(cad);
	int contador = 0;
	while (aux != NULL) {
		contador++;
		aux =  siguiente(aux, cad);
	}
	return contador;
}

bool esta_ordenada(cadena_t cad){
	bool res = true;
	if (!es_vacia_cadena(cad)) {
		localizador_t loc = inicio_cadena(cad);
		while (res && es_localizador(siguiente(loc,cad))) {
			localizador_t sig_loc =siguiente(loc,cad);
			if (numero_info(info_cadena(loc, cad)) >
				numero_info(info_cadena(sig_loc, cad)))
				res = false;
			else
				loc = siguiente(loc, cad);
		}
	}
	return res;
}

cadena_t mezcla(cadena_t c1, cadena_t c2){
	assert(esta_ordenada(c1) && esta_ordenada(c2));
	rep_cadena* resultado = crear_cadena();
	if ((es_vacia_cadena(c1))&&(es_vacia_cadena(c2))){
		return resultado;
	}
	// Si c1 no es vacia, hago una copia
	if (!es_vacia_cadena(c1)){
        rep_cadena* cadena_aux;
        cadena_aux = copiar_segmento(inicio_cadena(c1), final_cadena(c1), c1);
		resultado = insertar_segmento_despues(cadena_aux, inicio_cadena(resultado), resultado);
		liberar_cadena(cadena_aux);
	}
	// Si c2 no es vacia, hago una copia
	if (!es_vacia_cadena(c2)){
        rep_cadena* cadena_aux2;
        cadena_aux2 = copiar_segmento(inicio_cadena(c2), final_cadena(c2), c2);
        resultado = insertar_segmento_despues(cadena_aux2, final_cadena(resultado), resultado);
		liberar_cadena(cadena_aux2);
	}
	// Si c1 y c2 no son vacias, ordeno. Quedan los elementos de c1 primero.
	if ((!es_vacia_cadena(c1))&&(!es_vacia_cadena(c2))) {
		nodo* aux1 = inicio_cadena(resultado);
		nodo* aux2;
		while (siguiente(aux1, resultado) != NULL){
			aux2 = siguiente(aux1, resultado);
			while (aux2 != NULL) {
				if (numero_info(info_cadena(aux2, resultado)) < numero_info(info_cadena(aux1, resultado))) {
					intercambiar(aux1, aux2, resultado);
				}
				aux2 = siguiente(aux2, resultado);
			}
			aux1 = siguiente(aux1, resultado);
		}
	}
	return resultado;
}
cadena_t concatenar(cadena_t c1, cadena_t c2){
	rep_cadena* cad2 = crear_cadena();
	if (es_vacia_cadena(c1) && es_vacia_cadena(c2)) {
		return cad2;
	} 
	if (inicio_cadena(c1) != NULL) {
		nodo* aux2 = inicio_cadena(c1);
		insertar_al_final(copia_info(info_cadena(aux2, c1)), cad2);
		aux2 = siguiente(aux2, c1);
		while (aux2 != NULL) {
			insertar_al_final(copia_info(info_cadena(aux2, c1)), cad2);
			aux2 = siguiente(aux2, c1);
		}
		if (inicio_cadena(c2) != NULL) {
			aux2 = inicio_cadena(c2);
			insertar_al_final(copia_info(info_cadena(aux2, c2)), cad2);
			aux2 = siguiente(aux2, c2);
			while (aux2 != NULL) {
				insertar_al_final(copia_info(info_cadena(aux2, c2)), cad2);
				aux2 = siguiente(aux2, c2);
			}
		} 
	} else {
		nodo* aux2 = inicio_cadena(c2);
		insertar_al_final(copia_info(info_cadena(aux2, c2)), cad2);
		aux2 = siguiente(aux2, c2);
		while (aux2 != NULL) {
			insertar_al_final(copia_info(info_cadena(aux2, c2)), cad2);
			aux2 = siguiente(aux2, c2);
		}
	}
	return cad2;
}
	

cadena_t ordenar(cadena_t cad){
	nodo* aux1 = inicio_cadena(cad);
	nodo* aux2;
	while (aux1 != NULL){
		aux2 = siguiente(aux1, cad);
		while (aux2 != NULL) {
			if (numero_info(info_cadena(aux2, cad)) < numero_info(info_cadena(aux1, cad))) {
				intercambiar(aux1, aux2, cad);
			}
			aux2 = siguiente(aux2, cad);
		}
		aux1 = siguiente(aux1, cad);
	}
    return cad;
}

cadena_t cambiar_todos(int original, int nuevo, cadena_t cad){
	if (inicio_cadena(cad) != NULL) {
		nodo* aux = inicio_cadena(cad);
		while (aux != NULL) {
			if (numero_info(info_cadena(aux, cad)) == original) {
				char* copia_frase = new char[strlen(frase_info(info_cadena(aux, cad))) + 1];
				strcpy(copia_frase, frase_info(info_cadena(aux, cad)));
				info_t aux2 = info_cadena(aux, cad);
				liberar_info(aux2);
				info_t i = crear_info(nuevo, copia_frase);
				cambiar_en_cadena(i, aux, cad);
			}
			aux = siguiente(aux, cad);
		}
	}
    return cad;
}





