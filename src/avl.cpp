/*
  Módulo de definición de `avl_t'.

  Se definen los árboles AVL de elementos de tipo `info_t'.
  La propiedad de orden de los árboles es definida por el dato numérico de sus
  elementos.

  Laboratorio de Programación 2.
  InCo-FIng-UDELAR
 */
#include "../include/avl.h"
#include "../include/cola_avls.h"
#include "../include/pila.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

// Representación de `avl_t'.
struct rep_avl{
    info_t dato;
    nat altura;
    nat cantidad;
    avl_t izq, der;
};

/* Funciones y procedimientos auxiliares */
static info_t *en_orden_rec(info_t *res, nat &tope, avl_t avl) {
  if(!es_vacio_avl(avl)) {
    res = en_orden_rec(res, tope, izq_avl(avl));
    res[tope] = raiz_avl(avl);
    tope++;
    res = en_orden_rec(res, tope, der_avl(avl));
  }
  return res;
}

static avl_t a2avl_rec(info_t *infos, int inf, int sup) {
  avl_t res;
  if (inf > sup) {
    res = NULL;
  } else {
    nat medio = (inf + sup) / 2;
    res = new rep_avl;
    res->dato = infos[medio];
    res->izq = a2avl_rec(infos, inf, medio - 1);
    res->der = a2avl_rec(infos,medio + 1, sup);
    res->altura = 0;
    res->cantidad = 0;
    // ajustar res->altura y res->cantidad;
  }
  return res;
}

struct avl_ultimo {
  avl_t avl;
  int ultimo;
};

static avl_ultimo avl_min_rec(nat h, nat primero) {
  avl_ultimo res;
  if (h == 0) {
    res.avl = NULL;
    res.ultimo = primero - 1;
  } else if (h == 1) {
    //completar
  } else {
    // completar
  }
  return res;
}

static void rot_izq(avl_t &avl) {
  avl->altura = altura_de_avl(der_avl(avl));
  avl->der->altura++;
  nat nodos_der = cantidad_en_avl(der_avl(avl));
  avl->der->cantidad = cantidad_en_avl(avl);
  avl->cantidad = nodos_der;
  avl_t avl_a_rotar = der_avl(avl);
  avl->der = avl_a_rotar->izq;
  avl_a_rotar->izq = avl;
  avl = avl_a_rotar;
}

static void rot_der(avl_t &avl) {
  avl->altura = altura_de_avl(izq_avl(avl));
  avl->izq->altura++;
  nat nodos_izq = cantidad_en_avl(izq_avl(avl));
  avl->izq->cantidad = cantidad_en_avl(avl);
  avl->cantidad = nodos_izq;
  avl_t avl_a_rotar = izq_avl(avl);
  avl->izq = avl_a_rotar->der;
  avl_a_rotar->izq = avl;
  avl = avl_a_rotar;
}

static void balancear_avl(avl_t &avl) {
  int FE = (altura_de_avl(der_avl(avl)) - altura_de_avl(izq_avl(avl)));
  if (FE < -1) {
    rot_izq(avl);
  } else if (FE > 1){
    rot_der(avl);
  }
}

static nat maximo(nat n1, nat n2) {return (n1>= n2) ? n1 : n2;}

/*
  Devuelve un avl_t vacío (sin elementos).
  El tiempo de ejecución es O(1).
 */
avl_t crear_avl(){ return NULL; }

/*
  Devuelve `true' si y sólo si `avl' es vacío (no tiene elementos).
  El tiempo de ejecución es O(1).
 */
bool es_vacio_avl(avl_t avl){ return avl == NULL; }

/*
  Inserta `i' en `avl' respetando la propiedad de orden definida.
  Precondición: es_vacio_binario(buscar_en_avl(numero_info(i), avl).
  Precondición: numero_info(i) != INT_MAX.
  El tiempo de ejecución es O(log n), donde `n' es la cantidad de elementos
  de `avl'.
*/
void insertar_en_avl(info_t i, avl_t &avl){
  if (es_vacio_avl(avl)) {
    avl_t nuevo = new rep_avl;
    nuevo->dato = i;
    nuevo->der = NULL;
    nuevo->izq = NULL;
    nuevo->altura = nuevo->cantidad = 1;
    avl = nuevo;
  }else if (numero_info(i) < numero_info(raiz_avl(avl))) {
    insertar_en_avl(i, avl->izq);
    avl->cantidad++;
    avl->altura = maximo(altura_de_avl(izq_avl(avl)), altura_de_avl(der_avl(avl))) + 1;
    // balancer en caso que sea requerido
    balancear_avl(avl);
  } else if (numero_info(i) > numero_info(raiz_avl(avl))) {
    insertar_en_avl(i, avl->der);
    avl->cantidad++;
    avl->altura = maximo(altura_de_avl(izq_avl(avl)), altura_de_avl(der_avl(avl))) + 1;
    // balancer en caso que sea requerido
    balancear_avl(avl);
  }
}

/*
  Devuelve el subárbol que tiene como raíz al nodo con el elemento cuyo dato
  numérico es `clave'.
  Si ningún nodo cumple esa condición devuelve el árbol vacío.
  El tiempo de ejecución es O(log n), donde `n' es la cantidad de elementos de
  `avl'.
 */
avl_t buscar_en_avl(int clave, avl_t avl){
  avl_t res;
  if (es_vacio_avl(avl)) {
    res = crear_avl();
  } else {
    if (clave < numero_info(raiz_avl(avl))) {
      res = buscar_en_avl(clave, izq_avl(avl));
    } else if (clave > numero_info(raiz_avl(avl))) {
      res = buscar_en_avl(clave, der_avl(avl));
    } else {
      res = avl;
    }
  }
  return res;
}

/*
  Devuelve el elemento asociado a la raíz de `avl'.
  Precondición: ! es_vacio_binario(b).
  El tiempo de ejecución es O(1).
 */
info_t raiz_avl(avl_t avl){
  assert(!es_vacio_avl(avl));
  return avl->dato; 
}

/*
  Devuelve el subárbol izquierdo de `avl'.
  Precondición: ! es_vacio_avl(avl).
  El tiempo de ejecución es O(1).
 */
avl_t izq_avl(avl_t avl){
  assert(!es_vacio_avl(avl));
  return avl->izq;
}

/*
  Devuelve el subárbol derecho de `avl'.
  Precondición: ! es_vacio_avl(avl).
  El tiempo de ejecución es O(1).
 */
avl_t der_avl(avl_t avl){
  assert(!es_vacio_avl(avl));
  return avl->der;
}

/*
  Devuelve la cantidad de elementos en `avl'.
  El tiempo de ejecución es O(1).
 */
nat cantidad_en_avl(avl_t avl){
  if (es_vacio_avl(avl)) {
    return 0;
  } else {
    return avl->cantidad;
  }
}

/*
  Devuelve la altura de  `avl'.
  La altura de un árbol vacío es 0.
  El tiempo de ejecución es O(1).
 */
nat altura_de_avl(avl_t avl){
  if (es_vacio_avl(avl)) {
    return 0;
  } else {
    return avl->altura;
  }
}

/*
  Devuelve un arreglo con los elementos de `avl'.
  Los elementos en el arreglo deben estar en orden creciente según los datos
  numericos.
 */
info_t *en_orden_avl(avl_t avl){
  info_t *res;
  if (es_vacio_avl(avl)) {
    res = NULL;
  } else {
    res = new info_t[cantidad_en_avl(avl)];
    nat tope = 0;
    res = en_orden_rec(res, tope, avl);
  }
  return res;
}

/*
  Devuelve un avl_t con los `n' elementos que están en el rango [0 .. n - 1]
  del arreglo `infos'.
  Los elementos están ordenados de manera creciente estricto (creciente y sin
  repetidos) según los datos numércos.
  En cada nodo del árbol resultado la cantidad de elementos de su subárbol
  izquierdo es igual a, o 1 menos que, la cantidad de elementos de su subárbol
  derecho.
  El tiempo de ejecución es O(n).
 */
avl_t arreglo_a_avl(info_t *infos, nat n){
  return a2avl_rec(infos, 0, n - 1);
}

/*
  Devuelve un avl_t de altura `h' con `n' nodos, siendo `n' la mínima cantidad
  de elementos que puede tener un avl de altura `h'.
  Los datos numéricos de los elementos van desde 1 hasta `n'.
  El dato de texto de cada elemento es la cadena vacia.
  En ningún nodo puede ocurrir que el subárbol derecho tenga más nodos que el
  subárbol izquierdo.
  El tiempo de ejecución es O(n).
  Ver ejemplos en la letra y en el caso 408.
 */
avl_t avl_min(nat h){
  avl_ultimo res = avl_min_rec(h, 1);
  return res.avl;
}

/*
  Imprime los datos numéricos de los nodos de cada nivel de `avl'.
  Imprime una línea por nivel, primero el más profundo y al final el que
  corresponde a la raíz.
  Cada nivel se imprime de izquierda a derecha. Se debe dejar un espacio en
  blanco después de cada número.
  El tiempo de ejecución es O(n), siendo `n' la cantidad de nodos de `avl'.
  Ver ejemplos en la letra y en el caso 404.
 */
void imprimir_avl(avl_t avl){
   //Apilo todos los elementos del arbol en dos pilas auxiliares: una para los datos y otra para las alturas(O(n))
  cola_avls_t cola = crear_cola_avls();
  pila_t pila_datos = crear_pila(cantidad_en_avl(avl));
  pila_t pila_alturas = crear_pila(avl->cantidad);
  avl_t aux;
  encolar(avl, cola);
  while (!es_vacia_cola_avls(cola)){
    aux = frente(cola);
    desencolar(cola);
    apilar(numero_info(raiz_avl(aux)), pila_datos);
    apilar(altura_de_avl(aux), pila_alturas);
    if (!es_vacio_avl(der_avl(aux)))
      encolar(der_avl(aux), cola);
    if (!es_vacio_avl(izq_avl(aux)))
      encolar(izq_avl(aux), cola);
  }
  //usando las colas y una variable int que controla cuando son niveles distintos, imprimo el arbol(O(n))
  int control_altura = 1;
  while(!es_vacia_pila(pila_datos)){
    if(control_altura == cima(pila_alturas)){
      printf("%i ", cima(pila_datos));
      desapilar(pila_alturas);
      desapilar(pila_datos);
    } else {
      //imprimo salto de linea y el numero y un espacio
      printf("\n");
      printf("%i ", cima(pila_datos));
      control_altura = cima(pila_alturas);
      desapilar(pila_alturas);
      desapilar(pila_datos);
    }
  }
  printf("\n");
  liberar_cola_avls(cola);
  liberar_pila(pila_alturas);
  liberar_pila(pila_datos);
}


/*
  Libera la memoria asignada a `avl' y la de sus elementos.
  El tiempo de ejecución es O(n), donde `n' es la cantidad de elementos de
  `avl'.
 */
void liberar_avl(avl_t &avl){
  if (!es_vacio_avl(avl)) {
    liberar_avl(avl->izq);
    liberar_avl(avl->der);
    liberar_info(avl->dato);
    delete avl;
  }
}
