/*
  Módulo de definición de `conjunto_t'.

  Se define un conjunto de elementos de tipo `info_t'.
  No puede haber dos elementos con el mismo dato numérico.

  Laboratorio de Programación 2.
  InCo-FIng-UDELAR
 */

#include "../include/conjunto.h"

// Declaración del tipo `conjunto_t'
// struct rep_conjunto es la representación de `conjunto_t',
// definida en conjunto.cpp
struct rep_conjunto{
  avl_t arbol;
};

/* Constructoras */

/*
  Devuelve un conjunto_t vacío (sin elementos).
  El tiempo de ejecución es O(1).
 */
conjunto_t crear_conjunto(){
  conjunto_t result = new rep_conjunto;
  return result;
}

/*
  Devuelve un conjunto_t cuyo único elemento es `i'.
  El tiempo de ejecución es O(1).
 */
conjunto_t singleton(info_t i){
  conjunto_t result = new rep_conjunto;
  return result;
}

//Funcion auxiliar para union_conjunto
void copiar_arbol(avl_t a_copiar, avl_t &resultado, bool es_c2){
  if(!es_vacio_avl(a_copiar)){
    if(!es_c2){
      insertar_en_avl(copia_info(raiz_avl(a_copiar)),resultado);
      copiar_arbol(izq_avl(a_copiar), resultado, es_c2);
      copiar_arbol(der_avl(a_copiar), resultado, es_c2);
    } else{
      if(es_vacio_avl(buscar_en_avl(numero_info(raiz_avl(a_copiar)), resultado))){
        insertar_en_avl(copia_info(raiz_avl(a_copiar)),resultado);
        copiar_arbol(izq_avl(a_copiar), resultado, es_c2);
        copiar_arbol(der_avl(a_copiar), resultado, es_c2);
      }
    }
  }
}
/*
  Devuelve un conjunto_t con los elementos que pertenecen a  `c1' o `c2'.
  Si en ambos conjuntos hay un elemento con el mismo dato  numérico en el
  conjunto_t devuelto se debe incluir el que pertenece a `c1'.
  El tiempo de ejecucion es O(n1 + n2 + n.log n), siendo `n1' y `n2' la
  cantidad de elementos de `c1' y `c2' respectivamente y `n' la del conjunto_t
  resultado.
  El conjunto_t devuelto no comparte memoria ni con `c1' no con `c2'.
 */
conjunto_t union_conjunto(conjunto_t c1, conjunto_t c2){
  conjunto_t res = new rep_conjunto;
  avl_t resultado = crear_avl();
  //Copio arbol c1 completo
  copiar_arbol(c1->arbol, resultado, false);
  //para cada nodo de c2, si el nodo no esta en la copia de c1 -> hago una copia y la inserto 
  copiar_arbol(c2->arbol, resultado, true);
  res->arbol = resultado;
  return res;
}
//funcion auxiliar para diferencia
void copiar_diferentes(avl_t c1, avl_t c2, avl_t &resultado){
  if(!es_vacio_avl(c1)){
    if(es_vacio_avl(buscar_en_avl(numero_info(raiz_avl(c1)), c2))){
      insertar_en_avl(copia_info(raiz_avl(c1)),resultado);
      copiar_diferentes(izq_avl(c1), c2, resultado);
      copiar_diferentes(der_avl(c1), c2, resultado);
    }
  }
}
/*
  Devuelve un conjunto_t con los elementos de `c1' cuyos datos numéricos no
  pertenecen a `c2'.
  El tiempo de ejecucion es O(n1 + n2 + n.log n), siendo `n1' y `n2' la
  cantidad de elementos de `c1' y `c2' respectivamente y `n' la del conjunto_t
  resultado.
  El conjunto_t devuelto no comparte memoria ni con `c1' no con `c2'.
 */
conjunto_t diferencia(conjunto_t c1, conjunto_t c2){
  conjunto_t res= new rep_conjunto;
  //para cada nodo de c1, busco el nodo en c2, si no esta en c2 hago una copia y lo inserto en un nuevo arbol
  avl_t resultado = crear_avl();
  copiar_diferentes(c1->arbol, c2->arbol, resultado);
  res->arbol = resultado;
  return res;
}

/*
  Libera la memoria asignada a `c' y la de todos sus elementos.
  El tiempo de ejecución es O(n), siendo `n' la cantidad de elementos de `c'.
 */
void liberar_conjunto(conjunto_t &c){

}

/*
  Devuelve `true' si y sólo si `info' es un elemento de `c'.
  El tiempo de ejecución es O(log n), siendo `n' es la cantidad de
  elementos de `c'.
 */
bool pertenece_conjunto(info_t info, conjunto_t s){
  return true;
}

/*
  Devuelve `true' si y sólo `c' es vacío (no tiene elementos).
  El tiempo de ejecución es O(1).
 */
bool es_vacio_conjunto(conjunto_t c){
  return true;
}

/*
  Devuelve un conjunto_t con los `n' elementos que están en en el rango
  [0 .. n - 1] del arreglo `infos'.
  Los elementos están ordenados de manera creciente estricto (creciente y sin
  repetidos) según los datos numércos.
  El tiempo de ejecución es O(n).
 */
conjunto_t arreglo_a_conjunto(info_t *infos, nat n){
  conjunto_t result = new rep_conjunto;
  return result;
}

/*
  Devuelve un iterador_t de los elementos de `c'.
  En la recorrida del iterador devuelto los datos numéricos aparecerán en orden
  creciente.
  El tiempo de ejecución es O(n), siendo `n' es la cantidad de elementos de `c'.
  El iterador_t resultado NO comparte memoria con `c'.
 */
iterador_t iterador_conjunto(conjunto_t c){
  //IMPLEMENTADO EN IMAGEN
  iterador_t res = crear_iterador();
  info_t *infos = en_orden_avl(c->arbol);
  for (nat i = 0; i < cantidad_en_avl(c->arbol); i++)
    agregar_a_iterador(infos[i], res);
  bloquear_iterador(res);
  delete[] infos;
  return res;
}
