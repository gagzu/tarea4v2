/*5000304 6182649 */
#include "../include/info.h"
#include "../include/cadena.h"
#include "../include/binario.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

struct rep_binario {
	info_t dato;
	rep_binario *izq;
	rep_binario *der;
};

binario_t crear_binario() { return NULL; }

binario_t insertar_en_binario(info_t i, binario_t b){
	if (es_vacio_binario(b)){
		binario_t nuevo = new rep_binario;
		nuevo->dato = i;
		nuevo->der = NULL;
		nuevo->izq = NULL;
		b = nuevo;
		return b;
	} else if (strcmp(frase_info(i), frase_info(raiz(b))) < 0) {
		b->izq = insertar_en_binario(i, izquierdo(b));
	} else if (strcmp(frase_info(i), frase_info(raiz(b))) > 0){
		b->der = insertar_en_binario(i, derecho(b));
	}
	return b;
}

binario_t remover_mayor(binario_t b) {
	assert(!es_vacio_binario(b));
	if (es_vacio_binario(derecho(b))) {
		binario_t izq = izquierdo(b);
		delete (b);
		b = izq;
    } else {
		b->der = remover_mayor(derecho(b));
	}
	return b;
}

info_t mayor(binario_t b) {
	assert(!es_vacio_binario(b));
  while ( !es_vacio_binario(derecho(b)))
    b = derecho(b);
  return raiz(b);
}

binario_t remover_de_binario(const char *t, binario_t b){
    if (!es_vacio_binario(b)){
        if (strcmp(frase_info(raiz(b)), t) == 0) {
            binario_t a_eliminar = b;
            if (!es_vacio_binario(izquierdo(b)) && !es_vacio_binario(derecho(b))) {
            binario_t ancestro_de_mas_grande = NULL;
            binario_t mas_grande = izquierdo(b);
            while (!es_vacio_binario(derecho(mas_grande))) {
                ancestro_de_mas_grande = mas_grande;
                mas_grande = derecho(mas_grande);
            }
            if (!es_vacio_binario(ancestro_de_mas_grande)) {
                ancestro_de_mas_grande->der = izquierdo(mas_grande);
                mas_grande->izq = izquierdo(b);
            }
            mas_grande->der = derecho(b);
            b = mas_grande;
            }
            else if (!es_vacio_binario(izquierdo(b))) {
            b = izquierdo(b);
            }
            else if (!es_vacio_binario(derecho(b))) {
            b = derecho(b);
            }
            else {
            b = NULL;
            }
            liberar_info(a_eliminar->dato);
            delete a_eliminar;
            return b;
        }
        else if (strcmp(t, frase_info(raiz(b))) < 0) {
        b->izq = remover_de_binario(t, izquierdo(b));
        }
        else {
        b->der = remover_de_binario(t, derecho(b));
        }
        return b;
    }
    return NULL;
    
}


bool es_camino(cadena_t c, binario_t b){
    bool flag = true;
    localizador_t aux = inicio_cadena(c);
    rep_binario * aux_binario = b;
    
    if(es_vacia_cadena(c) && es_vacio_binario(b)){return true;}
    
    while(aux != NULL && flag ){
        if (es_vacio_binario(aux_binario)) return false;
        int son_iguales = strcmp(frase_info(info_cadena(inicio_cadena(c), c)), frase_info(raiz(b)));
        if(son_iguales == 0){
            if (siguiente(aux, c) != NULL){
                int direccion = strcmp(frase_info(info_cadena(siguiente(aux,c), c)), frase_info(raiz(b)));
                if (direccion < 0) {
                    aux_binario = izquierdo(aux_binario);
                } else{
                    aux_binario = derecho(aux_binario);
                }
                aux = siguiente(aux, c);
            } else {
                aux = siguiente(aux,c);
            }
        } else return false;
    }
    return  es_vacio_binario(izquierdo(aux_binario)) && es_vacio_binario(derecho(aux_binario)) && !es_localizador(aux);
    
}
binario_t liberar_binario(binario_t b){
  if (b != NULL){
      b->izq = liberar_binario(b->izq);
      b->der = liberar_binario(b->der);
      liberar_info(b->dato);
      delete b;
      b = NULL;
  }
  return b;
}

bool es_vacio_binario(binario_t b){
	return b == NULL;
}

int max(int x, int y){
  if (x >= y){
    return x;
  }
  else{
    return y;
  }
}

bool es_AVL_aux (binario_t b, int &altura){
  if (b == NULL)
    return true;
  int altura_izq = 0;
  int altura_der = 0;

  bool izq_AVL = es_AVL_aux(b->izq, altura_izq);
  bool der_AVL = es_AVL_aux(b->der, altura_der);

  altura = max(altura_izq, altura_der) + 1;

  if (abs(altura_izq - altura_der) > 1) {
    return false;
  }
  else if (!izq_AVL || !der_AVL)
    return false;
  else
    return true;
}

bool es_AVL(binario_t b){
  int altura;
  return es_AVL_aux(b, altura);
}

info_t raiz(binario_t b){
	assert(!es_vacio_binario(b));
	return b->dato;
}

binario_t izquierdo(binario_t b){
  assert (! es_vacio_binario(b));
  return b->izq;
}
binario_t derecho(binario_t b){
  assert (! es_vacio_binario(b));
  return b->der;
}
//CAMBIAR POR IMPLEMENTACION DEL EJEMPLO DE IMAGEN
binario_t buscar_subarbol(const char *t, binario_t b){
  binario_t res;
  if (es_vacio_binario(b))
    res = crear_binario();
  else {
    int comp = strcmp(t, frase_info(raiz(b)));
    if (comp < 0)
      res = buscar_subarbol(t, izquierdo(b));
    else if (comp > 0)
      res = buscar_subarbol(t, derecho(b));
    else 
      res = b;
  }
  return res;
}

static nat maximo(nat n1, nat n2) {return (n1>= n2) ? n1 : n2;}

nat altura_binario(binario_t b){
  nat altura;
  if (b == NULL){
    altura = 0;
  }else{ 
    altura = 1 + maximo(altura_binario(b->izq), altura_binario(b->der));
  }
  return altura; 
}

nat cantidad_binario(binario_t b){
  nat cantidad;
  if (b == NULL) {
    cantidad = 0;
  } else{
    cantidad = 1 + cantidad_binario(b->izq) + cantidad_binario(b->der);
  }
  return cantidad;
}

void linealizacion_aux(binario_t b, cadena_t &cad){
  if (b!=NULL) {
    linealizacion_aux(b->izq, cad); 
    insertar_al_final(copia_info(b->dato), cad);
    linealizacion_aux(b->der, cad);
  }
}

cadena_t linealizacion(binario_t b){
  cadena_t cad = crear_cadena();
  linealizacion_aux(b, cad);
  return cad;
}
void nivel_en_binario_aux (nat l, binario_t b, cadena_t &cad){
  if (b!=NULL){
    if(l == 1){
      insertar_al_final(copia_info(b->dato), cad);
    } else if (l>1){
      l = l-1;
      nivel_en_binario_aux(l, b->izq, cad);
      nivel_en_binario_aux(l, b->der, cad);
    }
  }
}

cadena_t nivel_en_binario (nat l, binario_t b){
  cadena_t cad = crear_cadena();
  nivel_en_binario_aux(l, b, cad);
  return cad;
}

void suma_ultimos_pares_aux(nat &i, binario_t b, int &suma){
  if ((i>0)&&(b!=NULL)){
    suma_ultimos_pares_aux(i, b->der, suma);
    if ((i>0)&&(numero_info(b->dato) % 2 == 0)){
      suma = suma + numero_info(b->dato);
      i = i - 1;
    }
    suma_ultimos_pares_aux(i, b->izq, suma);
  }
}

int suma_ultimos_pares(nat i, binario_t b){
  int suma = 0;
  suma_ultimos_pares_aux(i, b, suma);
  return suma;
}

binario_t menores(int clave, binario_t b){

  binario_t nuevo_arbol = crear_binario();

  if (b != NULL){
    if (numero_info(b->dato) < clave){
      nuevo_arbol = insertar_en_binario(copia_info(b->dato), nuevo_arbol);
    }

    binario_t filtro_izq = menores(clave, b->izq);
    binario_t filtro_der = menores(clave, b->der);

    if (filtro_izq != NULL && filtro_der == NULL) {
      if (nuevo_arbol == NULL) {
        nuevo_arbol = filtro_izq;
      }
      else {
        nuevo_arbol->izq = filtro_izq;
      }
    }
    else if (filtro_der != NULL && filtro_izq == NULL)  {
      if (nuevo_arbol == NULL) {
        nuevo_arbol = filtro_der;
      }
      else {
        nuevo_arbol->der = filtro_der;
      }
    }
    else if (filtro_izq != NULL && filtro_der != NULL) {
        if (numero_info(b->dato) < clave) {
          nuevo_arbol->izq = filtro_izq;
          nuevo_arbol->der = filtro_der;
        }
        else {
          binario_t ancestro_de_mas_grande = NULL;
          binario_t mas_grande = filtro_izq;

          while (mas_grande->der != NULL) {
            ancestro_de_mas_grande = mas_grande;
            mas_grande = mas_grande->der;
          }

          if (ancestro_de_mas_grande != NULL) {
            ancestro_de_mas_grande->der = mas_grande->izq;

            mas_grande->izq = filtro_izq;
          }

          mas_grande->der = filtro_der;
          
          nuevo_arbol = mas_grande;
        }
    }

  } 

  return nuevo_arbol;
}

void imprimir_binario_aux(binario_t b, int contador){
  if ( b != NULL){
    contador ++;
    imprimir_binario_aux(b->der, contador);
    printf("\n");
    for (int i=1; i<contador; i++) 
      printf("-");
    printf("(%i,%s)", numero_info(b->dato), frase_info(b->dato));
    imprimir_binario_aux(b->izq, contador);
  }
}

void imprimir_binario(binario_t b){
  int contador = 0;
  imprimir_binario_aux(b, contador);
  printf("\n");
}

binario_t cadena_a_binario(cadena_t cad) {
	binario_t nuevo_arbol = crear_binario();

	if (!(es_vacia_cadena(cad))) {
		if (inicio_cadena(cad) != final_cadena(cad)) {
			nuevo_arbol = new rep_binario;
			
			localizador_t aux = inicio_cadena(cad);
			localizador_t mitad = final_cadena(cad);

			while (es_localizador(siguiente(aux, cad))) {
				if (es_localizador(siguiente(siguiente(aux, cad), cad))) {
					mitad = anterior(mitad, cad);
					aux = siguiente(aux, cad);
				}
				aux = siguiente(aux, cad);
			}

			nuevo_arbol->dato = copia_info(info_cadena(mitad, cad));


			cadena_t izquierdo = NULL;
			if (es_localizador(anterior(mitad, cad))) {
				izquierdo = copiar_segmento(inicio_cadena(cad), anterior(mitad, cad), cad);
			} else {
				izquierdo = crear_cadena();
			}
			
      cadena_t derecho = NULL;
      if (es_localizador(siguiente(mitad, cad))) {
			  derecho = copiar_segmento(siguiente(mitad, cad), final_cadena(cad), cad);
			} else {
				derecho = crear_cadena();
			}


			nuevo_arbol->izq = cadena_a_binario(izquierdo);
			nuevo_arbol->der = cadena_a_binario(derecho);


			liberar_cadena(derecho);
			liberar_cadena(izquierdo);

		} else {
            nuevo_arbol = new rep_binario;
            nuevo_arbol->dato = copia_info(info_cadena(inicio_cadena(cad), cad));
			nuevo_arbol->izq = nuevo_arbol->der = NULL;
		}
	}
	return nuevo_arbol;
}
